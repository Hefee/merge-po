##!/usr/bin/env python3

import glob
import os
import pathlib
import shutil
import subprocess
import tempfile

def runTest(name):
    with tempfile.TemporaryDirectory() as d:
        shutil.copyfile(f"tests/{name}/base", f"{d}/base")
        shutil.copyfile(f"tests/{name}/pot", f"{d}/pot")
        shutil.copyfile(f"tests/{name}/local", f"{d}/local")
        shutil.copyfile(f"tests/{name}/remote", f"{d}/remote")
        cmd = ["./mergetool", f"{d}/base", f"{d}/local", f"{d}/remote"]
        print(subprocess.call(cmd))
        cmd = ["diff", "-u", f"{d}/local", f"tests/{name}/merged"]
        return subprocess.call(cmd)

for test in glob.glob("tests/*/base"):
    p = pathlib.Path(test)
    testname = p.parent.name
    if runTest(testname) == 0:
        print(f"Test({testname}) - successfull!")
    else:
        print(f"Test({testname}) - failed!")
